# შეიტანეთ სამი წერტილის კოორდინატები, განსაზღვრეთ გამოდგება თუ არა ისინი
# სამკუთხედის წვეროებად. თუ გამოდგება, დაადგინეთ სამკუთხედის ტიპი გვერდების და
# კუთხეების მიხედვით, წინააღმდეგ შემთხვევაში გამოიტანეთ შეტყობინება „False“ (გაითვალისწინეთ ფორმატი).
import math
x1 = float(input("x1 = "))
y1 = float(input("y1 = "))
x2 = float(input("x2 = "))
y2 = float(input("y2 = "))
x3 = float(input("x3 = "))
y3 = float(input("y3 = "))
a = math.sqrt(((x2-x1)**2)*(y2-y1)**2)
b = math.sqrt(((x3-x1)**2)*(y3-y1)**2)
c = math.sqrt(((x3-x2)**2)*(y3-y2)**2)
print(str("a")+"="+str("math.sqrt(((x2-x1)**2)*(y2-y1)**2)")+"="+str(a))
print(str("b")+"="+str("math.sqrt(((x3-x1)**2)*(y3-y1)**2)"+"="+str(b)))
print(str("c")+"="+str("math.sqrt(((x3-x2)**2)*(y3-y2)**2)")+"="+str(c))
print(str("a")+"+"+str("b")+"="+str(a+b))
print(str("b")+"+"+str("c")+"="+str(b+c))
print(str("c")+"+"+str("a")+"="+str(a+c))
if a+b > c and a+c > b and b+c > a and a**2 == b**2+c**2:
    print("სამკუთხედი მართკუთხაა")
if a+b > c and a+c > b and b+c > a and a**2 > b**2+c**2:
    print("სამკუთხედი ბლაგვკუთხაა")
if a+b > c and a+c > b and b+c > a and a**2 < b**2+c**2:
    print("სამკუთხედი მახვილკუთხაა")
else:
    print("False")
if a > 0 and b > 0 and c > 0 and a == b == c:
    print("სამკუთხედი გოლგვერდაა")
if a == b != c or b == c != a or a == c != b:
    print("სამკუთხედი ტოლფერდაა")
if a != b != c and b != c:
    print("სამკუთხედი სხვადასხვა გვერდაა")
