# იპოვეთ ორ წერტილს შორის მანძილი (ვექტორის სიგრძე), წერტილების კოორდინატები
# შეიტანეთ კლავიატურიდან (გაითვალისწინეთ ფორმატი)
import math
x = float(input(" x = "))
y = float(input(" y = "))
d = (x**2+y**2)**1/2
if d > 0:
    d = math.sqrt(x**2+y**2)
    print(d)
else:
    print("ERROR")
