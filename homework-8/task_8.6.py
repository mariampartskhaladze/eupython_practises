# 6.	გამოიტანეთ 4 შემთხვევითი მთელი რიცხვი,
# დაბეჭდეთ მათ შო¬რის მაქსიმალური და მინიმალური.
import random
x = []
for i in range(5):
    x.append(random.randint(1, 15))
print(f"numbers: {x}")
print(f"max number {max(x)}")
print(f"min number {min(x)}")
