# 16.	შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუა¬ლედი¬დან 15 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშ¬ვნელობას გაუც¬ვა¬ლეთ ადგილები).
# დაით¬ვა¬ლოთ რამდენია გამოტანილ რიცხვებს შორის 5-ის ჯერადი.
import random

a = int(input("enter number >"))
b = int(input("enter second number> "))
x = 0
for i in range(15):
    r = random.randint(a, b)

    print(f"number {r}")
    if a > b:
        print(f"ცვლადებია {b, a}")
    if r % 5 == 0:
        x = x + 1
print(f"5 - ის ჯერადი რიცხვების რაოდენობაა {x}")
