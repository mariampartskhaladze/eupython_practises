# 3.	განსაზღვრეთ სამი a, b, c მთელი ტიპის ცვლა¬დე¬ბი,მიანიჭეთ შემ¬თხვევითი რიცხვები.
# დაბეჭდეთ შემდეგი გამოსახულების მნიშვნელობები:
# ა) ((a+b))/c^2 ;  ბ) (a-b)*(c-b);  გ) a^4+√b+c;
import random
import math
a_int = random.randint(1, 5)
b_int = random.randint(1, 20)
c_int = random.randint(1, 30)
print(f"ცვლადის მნიშვნელობებია => {a_int, b_int, c_int}")
print(f" ა) ((a+b))/c^2  გამოსახულების მნიშვნელობაა = > {(a_int+b_int)/(c_int**2)}")
print(f" ბ) (a-b)*(c-b) გამოსახულების მნიშვნელობაა = > {(a_int-b_int)*(c_int-b_int)}")
print(f" გ) a^4+√b+c გამოსახულების მნიშვნელობაა = > {(a_int**4)+math.sqrt(b_int)+c_int}")

