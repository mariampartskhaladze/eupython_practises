# 15.	შეიტანეთ a და b მთელი რიცხვები,
# თუ a ნაკლებია b-ზე დაბეჭდეთ 10 შემთხვევითი რიცხვი [a; b] შუა¬ლე¬დიდან,
# თუ b ნაკლებია a-ზე დავბეჭდოთ 10 შემთხვევითი რიცხვი [b; a] შუა¬ლე¬¬¬დიდან.
import random
a = int(input("enter first number > "))
b = int(input("enter second number > "))
for i in range(10):
    if a < b:
        r = random.randint(a, b)
        print(f"number {r}")
    elif a > b:
        r = random.randint(b, a)
        print(f"this number {r}")
