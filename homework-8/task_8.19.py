# 19.	შეიტანეთ a და b მთელი რიცხვები,
# გამოიტანეთ [a; b] შუა¬ლედი¬დან 40 შემთხვევითი რიცხვი
# (თუ a მეტია b-ზე, ამ ორი ცვლადის მნიშ¬ვნელობას გაუც¬ვა¬ლეთ ადგილები).
# დაადგინეთ რამდენი ლუწი და რამდენი კენტი რიცხვია გამოტანილ რიც¬ხვებს შორის,
# გამოიტანეთ ლუწი და კენტი რიცხვების ჯამი,
# მი¬ღე¬ბულ ჯამებს შორის გამოიტანეთ 5 შემთხვევითი რიცხვი.
import random

a = int(input("enter number >"))
b = int(input("enter second number> "))
d = 0
c = 0
for i in range(4):
    r = random.randint(a, b)
    print(f"numbers {r}")
    if a > b:
        print(f"ცვლადებია {b, a}")
    if r % 2 == 0:
       d = d + 1
    if r % 2 != 0:
        c = c + 1
print(f"number of odd numbers {c}")
print(f"number of even numbers {d}")
