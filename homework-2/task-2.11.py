# შეიტანეთ სამნიშნა მთელი რიცხვი,დაბეჭდეთ რიცხვის ციფრები და ციფრების ჯამი, ცალცალკე სტრიქონზე.
n = 156
# პირველი ციფრი
a = n // 100
print(a)
# მეორე ციფრი
b = n // 10 % 10
print(b)
# მესამე ციფრი
c = n % 10
print(c)
# ციფრების ჯამი
print(str(a)+"+"+str(b)+"+"+str(c)+"="+str(a+b+c))
