# განსაზღვრეთ a ცვლადი, მიანიჭეთ მნიშვნელობა კლავიატურიდან, დაბეჭდეთ a რიცხვის
# 3-ზე გაყოფის შედეგად მიღებულ მთელი ნაწილი და ნაშთი.
a = 11
# a გაყოფილი 3 -ზე
print(str(a)+"/"+str(3)+"="+str(a/3))
# მთელი ნაწილი
print(a // 3)
# ნაშთი
print(a % 3)
