# შეიტანეთ მთელი რიცხვი თუ რიცხვი მდებარეობს [1; 7] შუალედში გამოიტანს შეტყობინებას
# იმის შესახებ თუ კვირის რომელი დღეა, სხვა შემთხვევაში გამოიტანეთ სიტყვა „ERROR!!!“.
a = int(input("a="))
if int(a) == 1:
    print("ორშაბათი")
if int(a) == 2:
    print("სამშაბათი")
if int(a) == 3:
    print("ოთხშაბათი")
if int(a) == 4:
    print("ხუთშაბათი")
if int(a) == 5:
    print("პარასკევი")
if int(a) == 6:
    print("შაბათი")
if int(a) == 7:
    print("კვირა")
if int(a) < 1 or int(a) > 7:
    print("ERROR")
