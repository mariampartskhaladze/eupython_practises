# შეიტანეთ მთელი დადებითი რიცხვი, დაადგინეთ რიცხვი არის თუ არა ერთნიშნა, ორნიშნა, სამნიშნა ან ოთხნიშნა
a = int(input("a="))
if -10 < int(a) < 10:
    print("a ერთნიშნაა")
if -100 < int(a) <= -10 or 10 <= int(a) < 100:
    print("a ორნიშნაა")
if -1000 < int(a) <= -100 or 100 <= int(a) < 1000:
    print("a სამნიშნაა")
if -10000 < int(a) <= -1000 or 1000 <= int(a) < 10000:
    print("a ოთხნიშნაა")
