# პროგრამა შუქნიშნის მოქმედების წესები, პროგრამამ
# უნდა უზ¬¬რუნველყოს შუქნიშნის მოქმედების წესების განმარტებები შესაბამისად
# (R)-წითელი, (Y)- ყვითელი, (G)–მწვანე.
x = input(" ფერი = ")
R = "წითელი"
Y = "ყვითელი"
G = "მწვანე"
if x == R:
    print("შეაჩერე მოძრაობა")
elif x == Y:
    print("მოემზადე მოძრაობის დასაწყებად")
elif x == G:
    print("გააგრძელე მოძრაობა")
else:
    print("ERROR!")
