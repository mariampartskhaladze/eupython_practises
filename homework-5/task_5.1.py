# 1.	შეიტანეთ მთელი რიცხვი, დაბეჭდეთ რიცხვის შესაბამისი კვირის დღე.
# მაგ: (1-Monday, 2-Tuesday, …).
# თუ რიცხვი არ წარ¬მო¬ადგენს კვირის დღეს,
# გამოიტანეთ შეცდომის შესახებ შეტ¬ყობინება.
n = int(input("n = "))
if n == 1:
    print("Monday")
elif n == 2:
    print("Tuesday")
elif n == 3:
    print("Wednesday")
elif n == 4:
    print("Thursday")
elif n == 5:
    print("Friday")
elif n == 6:
    print("Saturday")
elif n == 7:
    print("Sunday")
else:
    print("ERROR")
