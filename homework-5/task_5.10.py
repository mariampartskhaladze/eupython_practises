# 	დაბეჭდეთ y ცვლადის მნიშვნელობა თუ:
# y={(x+2a+t,როცა  x-a+t=3
# x-2a+t,როცა x-a+t=4
# x+3a+2t,როცა x-a+t=5
# x+a+t,სხვა შემთხვევაში)┤
# x, a და t ცვლადებში მნიშვნელობები შეიტანეთ კლავიატუ¬რი¬დან.
x = float(input("x = "))
a = float(input("a = "))
t = float(input("t = "))
if x-a+t == 3:
    print(x+2*a+t)
elif x-a+t == 4:
    print(x-2*a+t)
elif x-a+t == 5:
    print(x+3*a+2*t)
else:
    print(x+a+t)
