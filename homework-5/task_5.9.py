# დაბეჭდეთ y ცვლადის მნიშვნელობა თუ:
# y={(x+a,როცა  x-a=1
# 2x-a,როცა x-a=2
# 3x+a+1,როცა x-a=3)
# x და a ცვლადებში მნიშვნელობები შეიტანეთ კლავიატუ¬რიდან.
x = float(input("x = "))
a = float(input("a = "))
if x-a == 1:
    print(x+a)
elif x-a == 2:
    print(2*x - a)
elif x-a == 3:
    print(3*x + a + 1)
