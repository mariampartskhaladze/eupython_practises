# 26.	შეიტანეთ 12 მთელი რიცხვი for ციკლის საშუალებით,
# დაბეჭ¬დეთ შეტანილი ლუწი რიცხვების ჯამი,
# კენტი რიცხვების ნამრავლი და სამის ჯერადი რიცხვების რაოდენობა.
import math
x = []
even_num = []
odd_num = []
c = 0
for i in range(3):
    x.append(int(input(f"შეიტანეთ რიცხვი {i + 1} => ")))
    if x[i] % 3 == 0:
     c = c + 1
for m in x:
    if m % 2 == 0:
        even_num.append(m)
    if m % 2 != 0:
        odd_num.append(m)
print(f"ლუწი რიცხვების ჯამი {sum(even_num)}")
if len(odd_num) == 0:
    print(f"კენტი რიცხვების ნამრავლია {0}")
else:
    print(f"კენტი რიცხვების ნამრავლია {math.prod(odd_num)}")
print(f"სამის ჯერადი რიცვების რაოდენობაა {c}")
